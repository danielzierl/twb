from tqdm import tqdm
from dataclasses import dataclass
from threading import Thread
import time
import csv
from typing import List
import requests
import random

from stypes import Proxy


def get_proxies(config) -> list[Proxy]:
    proxies: list[Proxy] = []

    print("getting proxies")
    try:
        proxies.extend(get_own_proxies())

        print("Filtering " + str(len(proxies)) + " proxies.")
        proxies = filter_unique_proxies(proxies)
        print("Total proxies after filters: " + str(len(proxies)))
        random.shuffle(proxies)
        if len(proxies) < config.min_proxies:
            print("Not enough proxies, restarting")
            time.sleep(5)
            get_proxies(config)
        return proxies
    except Exception as e:
        time.sleep(1)
        print("RESTARTING GET PROXY: ", e)
        return get_proxies(config)


def postprocess_proxy_response(response, proxy_protocol, no_add_protocol=False) -> list[Proxy]:
    if response.status_code == 200:
        lines = response.text.split("\n")
        lines = [line.strip() if no_add_protocol else f"{proxy_protocol}://{line.strip()}" for line in lines if line.strip()]
        lines = [Proxy(proxy={"http": line, "https": line}, deactivated=False, num_success=0, num_fail=0) for line in lines]
        return lines
    return []


def get_own_proxies() -> list:
    proxies = []
    response = requests.get("http://130.162.225.47:8080/get-proxies")
    proxies.extend(postprocess_proxy_response(response, "", no_add_protocol=True))
    return proxies


def get_proxyscrape_proxies() -> list:
    proxies = []
    response = requests.get(f"https://api.proxyscrape.com/v2/?request=displayproxies&protocol=http&timeout={10000}&country=all&ssl=all&anonymity=all")
    proxies.extend(postprocess_proxy_response(response, "http"))
    time.sleep(0.5)
    response = requests.get(f"https://api.proxyscrape.com/v2/?request=displayproxies&protocol=socks4&timeout={10000}&country=all&ssl=all&anonymity=all")
    proxies.extend(postprocess_proxy_response(response, "socks4"))
    time.sleep(0.5)
    response = requests.get(f"https://api.proxyscrape.com/v2/?request=displayproxies&protocol=socks5&timeout={10000}&country=all&ssl=all&anonymity=all")
    proxies.extend(postprocess_proxy_response(response, "socks5"))

    return proxies


def get_speedx_proxies():
    """
 more info can be found here
 https://github.com/TheSpeedX/PROXY-List
    """
    proxies = []
    response_http = requests.get("https://raw.githubusercontent.com/TheSpeedX/SOCKS-List/master/http.txt")
    proxies.extend(postprocess_proxy_response(response_http, "http"))
    response_socks4 = requests.get("https://raw.githubusercontent.com/TheSpeedX/SOCKS-List/master/socks4.txt")
    proxies.extend(postprocess_proxy_response(response_socks4, "socks4"))
    response_socks5 = requests.get("https://raw.githubusercontent.com/TheSpeedX/SOCKS-List/master/socks5.txt")
    proxies.extend(postprocess_proxy_response(response_socks5, "socks5"))
    return proxies


# def get_private_with_pass_proxies(file: str):
#     with open(file) as csv_file:
#         csv_reader = csv.reader(csv_file, delimiter=':')
#         proxies = []
#         for row in csv_reader:
#             proxy_str = f"http://{row[2]}:{row[3]}@{row[0]}:{row[1]}"
#             proxies.append(Proxy(proxy={"http": proxy_str, "https": proxy_str}, deactivated=False, num_success=0, num_fail=0))
#     return proxies


def read_file_proxies(file: str):
    proxies = []
    with open(file) as f:
        lines = f.readlines()
        for line in lines:
            line = line.strip()
            line = f"http://{line}"
            proxies.append(Proxy(proxy={"http": line, "https": line}, deactivated=False, num_success=0, num_fail=0))
    return proxies


def get_proxy_from_file(file: str, with_pass: bool, protocol: str | None = "http"):
    proxies = []
    with open(file) as csv_file:
        if protocol == "" or protocol is None:
            for line in csv_file:
                line = line.strip()
                proxies.append(Proxy(proxy={"http": line, "https": line}, deactivated=False, num_success=0, num_fail=0))
            return proxies

        assert protocol is not None
        csv_reader = csv.reader(csv_file, delimiter=':')
        for row in csv_reader:
            if with_pass:
                proxy_str = f"{protocol}://{row[2]}:{row[3]}@{row[0]}:{row[1]}"
            else:
                proxy_str = f"{protocol}://{row[0]}:{row[1]}"
            proxies.append(Proxy(proxy={"http": proxy_str, "https": proxy_str}, deactivated=False, num_success=0, num_fail=0))
    return proxies


def proxy_union(proxies1: list, proxies2: list) -> list:
    print(proxies1)
    print(proxies2)
    all_proxies = proxies1
    all_proxies.extend(proxies2)
    proxies = []
    seen = set()
    for proxy in all_proxies:
        if proxy['http'] not in seen:
            proxies.append(proxy)
            print(proxy['http'])
            seen.add(proxy['http'])
    return proxies


def filter_unique_proxies(proxies):
    out = []
    seen = set()
    for proxy in proxies:
        if proxy.proxy['http'] not in seen:
            out.append(proxy)
            seen.add(proxy.proxy['http'])
    print(f"Filtering unique proxies, {len(proxies)} -> {len(out)}")
    return out


def filter_proxies(proxies, config) -> list:
    out = []
    threads = []
    stats = {"good_proxies": 0, "bad_proxies": 0}

    def filter_inner():
        for _ in range(1):
            try:
                response = requests.get("https://twitch.tv", proxies=proxy, timeout=5)
                if response.status_code == 200:
                    stats["good_proxies"] += 1

                    out.append(proxy)
                    return
            except Exception as e:
                pass
        stats["bad_proxies"] += 1

    for proxy in tqdm(proxies):
        thread = Thread(target=filter_inner)
        thread.start()
        threads.append(thread)
        time.sleep(1 / config.pps)

    for thread in threads:
        thread.join()

    print(f"GOOD PROXIES: {stats['good_proxies']}, BAD PROXIES: {stats['bad_proxies']}, TOTAL: {len(proxies)}")
    return out
