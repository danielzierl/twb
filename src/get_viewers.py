import requests


def get_oauth(client_id, client_secret):
    url = "https://id.twitch.tv/oauth2/token"
    data = {
        "client_id": client_id,
        "client_secret": client_secret,
        "grant_type": "client_credentials"
    }
    response = requests.post(url, data=data)
    return response.json()["access_token"]


def get_viewers(oauth, client_id, channel_name):
    url = f"https://api.twitch.tv/helix/streams?user_login={channel_name}"
    headers = {
        "Authorization": f"Bearer {oauth}",
        "Client-Id": client_id
    }
    response = requests.get(url, headers=headers)

    try:
        count = response.json()["data"][0]["viewer_count"]
    except Exception:
        count = 0
    return count
