
import json
from math import floor
from typing import Any
from requests.exceptions import ProxyError
from tqdm import tqdm
import time
import datetime
import requests
from threading import Thread, Semaphore
from streamlink.session import Streamlink
from urllib3.exceptions import NewConnectionError
from fake_useragent import UserAgent
import os
from get_viewers import get_oauth, get_viewers
from proxy_getter import get_proxies
import threading

from stypes import Config, Proxy, Stats

PROXY_RE_VIEW_TIME_SEC = 30


def add_stat(stat: Stats, good: bool):
    with stat.lock:
        if good:
            stat.good_proxies += 1
        else:
            stat.bad_proxies += 1


def run_bot(config: Config):
    print("Running twitch view bot")
    thread_semaphore = Semaphore(config.nb_of_threads)  # Semaphore to control thread count
    user_agent = UserAgent()
    session = get_streamlink_session(user_agent)
    proxies: list[Proxy] = []
    stats: Stats | None = None
    viewers: int = 0
    if (config.fetch_viewer_count):
        client_id = os.getenv("CLIENT_ID")
        client_secret = os.getenv("CLIENT_SECRET")
        oauth_token = get_oauth(client_id, client_secret)
        print("OAuth token: " + oauth_token)
    my_proxies = []

    if (os.path.exists("myproxies_secret.json")):
        with open("myproxies_secret.json") as f:
            my_proxies = json.load(f)
    while True:
        loop_start = datetime.datetime.now()
        if (config.fetch_viewer_count):
            viewers = get_viewers(oauth_token, client_id, config.channel_name)
        # if elapsed_seconds > 300 or len(proxies) == 0:
        proxies = get_proxies(config)
        stats = Stats(lock=threading.Lock(), good_proxies=0, bad_proxies=0)

        assert (stats is not None)
        threads = []
        for i, proxy in enumerate(pbar := tqdm(proxies[:config.max_proxy])):
            if config.max_view_count > 0 and stats.good_proxies >= config.max_view_count:
                time_since_start = datetime.datetime.now() - loop_start
                if time_since_start < datetime.timedelta(seconds=PROXY_RE_VIEW_TIME_SEC):
                    to_sleep = PROXY_RE_VIEW_TIME_SEC - time_since_start.total_seconds()
                    print(f"Sleeping for {to_sleep} seconds")
                    time.sleep(to_sleep)
                break
            try:
                perc = 100 * stats.good_proxies / (stats.good_proxies + stats.bad_proxies)
                pbar.set_description(f"views: {viewers}, {stats.good_proxies}/{stats.bad_proxies}({perc:.2f}%~{floor(perc / 100 * len(proxies))})")
            except ZeroDivisionError:
                pbar.set_description("")
            if proxy.deactivated:
                continue
            thread_semaphore.acquire()
            thread = Thread(target=on_new_thread, args=(config, proxy, user_agent, thread_semaphore, session, stats, my_proxies, i),)
            thread.daemon = True
            thread.start()
            threads.append(thread)
            time.sleep(1 / config.pps)


def on_new_thread(config, proxy: Proxy, user_agent, semaphore, session, stats, proxies: list[Any], index: int):
    # Open the stream URL using the given proxy
    headers = {'User-Agent': user_agent.random}
    url = get_url(f"https://www.twitch.tv/{config.channel_name}", session, proxies, index)
    if not url:
        semaphore.release()
        return

    try:
        send_request(url, proxy, headers, stats)
    except requests.exceptions.Timeout as e:
        add_stat(stats, good=False)
        return
        # pass
    except requests.exceptions.ConnectionError as e:
        # print(e)
        add_stat(stats, good=False)
    except Exception as e:
        # print(e)
        add_stat(stats, good=False)
        proxy.num_fail += 1
        pass
    finally:
        semaphore.release()  # Release the semaphore


def send_request(url, proxy: Proxy, headers, stats):
    # send requests to the twitch service
    response = requests.head(url, proxies=proxy.proxy, headers=headers, timeout=10, verify=True, allow_redirects=False, stream=True)
    response.raise_for_status()
    if response.status_code == 200:
        add_stat(stats, good=True)
        proxy.num_success += 1
        return response
    raise Exception("failed to send request")


def get_url(channel_url, session, my_proxies: list[Any], index: int):
    if (my_proxies is not None):
        if (index % (len(my_proxies) + 1) != 0):
            session.set_option("http-proxy", my_proxies[index % len(my_proxies)])
        # print(my_proxies[index % len(my_proxies)])
    # else:
    #     print("none")

        # Retrieve the URL for the channel's stream
    try:
        url = ""
        streams = session.streams(channel_url)
    except Exception as e:
        print("Could not fetch stream url from streamlink. " + str(e))
        return ""

    # fetch didnt work
    if (streams is None):
        return ""

    # fetch worked but no relevant data
    if ('audio_only' not in streams and 'worst' not in streams):
        print("No audio_only or worst in streams.")
        print(streams)
        return ""

    # unpack
    if ('audio_only' in streams):
        url = streams['audio_only'].url
    else:
        url = streams['worst'].url

    return url


def get_streamlink_session(user_agent):
    session = Streamlink()
    session.set_option("http-headers", {
        "Accept-Language": "en-US,en;q=0.5",
        "Connection": "keep-alive",
        "DNT": "1",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent": user_agent.random,
        "Client-ID": "ewvlchtxgqq88ru9gmfp1gmyt6h2b93",
        "Referer": "https://www.google.com/"
    })
    return session
