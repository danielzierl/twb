import argparse
from view_bot_new import Config, run_bot
import signal
from dotenv import load_dotenv
import resource
import multiprocessing


def main(config: Config):
    while True:
        try:
            run_bot(config)
        except Exception as e:
            print("CRITICAL ERROR OCCURRED, RESTARTING WHOLE PROGRAM: ", e)


def sig_handler(signal, frame):
    print("EXITING PROGRAM...")
    exit(0)


def set_ulimit_n(limit):
    resource.setrlimit(resource.RLIMIT_NOFILE, (limit, limit))


if __name__ == '__main__':
    signal.signal(signal.SIGINT, sig_handler)
    parser = argparse.ArgumentParser()
    parser.add_argument('--threads', '-t', type=int, help='Number of threads')
    parser.add_argument('--name', '-n', type=str, help='Twitch channel name')
    parser.add_argument('--proxy-per-second', '-pps', type=int, help='How many proxies are gonna send req per second')
    parser.add_argument('--max-proxy', '-m', type=int, help='Max number of proxies', required=False)
    parser.add_argument("--processes-count", "-pc", type=int, help="Number of processes")
    parser.add_argument("--max_view-count", "-mvc", type=int, help="Max number of viewers", required=False)
    parser.add_argument("--min-proxies", "-mp", type=int, help="Min number of proxies", required=False)
    parser.add_argument("--fetch-viewer-count", "-fvc", action="store_true", help="Fetch viewer count", required=False)
    args = parser.parse_args()
    nb_of_threads = args.threads or 100
    channel_name = args.name
    timeout = args.proxy_per_second or 100
    max_proxy = args.max_proxy
    max_view_count = args.max_view_count or -1
    min_proxies = args.min_proxies or 0
    fetch_viewer_count = args.fetch_viewer_count

    set_ulimit_n(args.threads * 2 or 10000)

    print("#" * 100)
    print(f"Threads: {nb_of_threads} ")
    print(f"Channel name: {channel_name} ")
    print(f"Proxy per second: {timeout} ")
    print(f"Maximum proxies: {max_proxy} ")
    print(f"Maximum viewers: {max_view_count if max_view_count > 0 else max_view_count} ")
    print(f"Minimum proxies: {min_proxies} ")
    print("#" * 100)
    print("")
    config: Config = Config(
        pps=timeout,
        nb_of_threads=nb_of_threads,
        channel_name=channel_name,
        max_proxy=max_proxy,
        max_view_count=max_view_count,
        min_proxies=min_proxies,
        fetch_viewer_count=fetch_viewer_count
    )
    load_dotenv()
    for _ in range(args.processes_count or 1):
        process = multiprocessing.Process(target=main, args=(config,))
        process.start()
