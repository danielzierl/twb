from dataclasses import dataclass
import threading


@dataclass
class Config:
    pps: int
    nb_of_threads: int
    channel_name: str
    max_proxy: int
    max_view_count: int
    min_proxies: int
    fetch_viewer_count: bool


@dataclass
class Stats:
    lock: threading.Lock
    good_proxies: int
    bad_proxies: int


@dataclass
class Proxy:
    proxy: dict[str, str]
    deactivated: bool
    num_success: int
    num_fail: int
