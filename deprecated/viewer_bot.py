import time
import random
import datetime
import requests
from sys import exit
from threading import Thread, Semaphore
from streamlink import Streamlink
from fake_useragent import UserAgent
from requests import RequestException
import csv


class ViewerBot:
    def __init__(self, nb_of_threads, channel_name, proxylist, proxy_imported, pps, stop=False, type_of_proxy="http", max_proxy=-1):
        self.nb_of_threads = nb_of_threads
        self.nb_requests = 0
        self.stop_event = stop
        self.proxylist = proxylist
        self.all_proxies = []
        self.proxyrefreshed = True
        self.max_proxy = max_proxy
        try:
            self.type_of_proxy = type_of_proxy.get()
        except:
            self.type_of_proxy = type_of_proxy
        self.proxy_imported = proxy_imported
        self.pps = pps
        self.channel_url = "https://www.twitch.tv/" + channel_name.lower()
        self.proxyreturned1time = False
        self.thread_semaphore = Semaphore(int(nb_of_threads))  # Semaphore to control thread count

    def create_session(self):
        # Create a session for making requests
        self.ua = UserAgent()
        self.session = Streamlink()

        self.session.set_option("http-headers", {
            "Accept-Language": "en-US,en;q=0.5",
            "Connection": "keep-alive",
            "DNT": "1",
            "Upgrade-Insecure-Requests": "1",
            "User-Agent": self.ua.random,
            "Client-ID": "ewvlchtxgqq88ru9gmfp1gmyt6h2b93",
            "Referer": "https://www.google.com/"
        })
        return self.session

    def make_request_with_retry(self, session, url, proxy, headers, proxy_used, max_retries=3):
        for _ in range(max_retries):
            try:
                # send requests to the twitch service
                response = session.head(url, proxies=proxy, headers=headers, timeout=10)
                if response.status_code == 200:

                    return response
                else:
                    return None
            except RequestException as e:
                # print("error", e)
                continue

        return None

    def get_proxies(self):
        lines = []

        if self.proxylist == None or self.proxyrefreshed == False:
            print("getting proxies")
            try:
                with open("proxies.txt", "r") as f:
                    lines_temp = f.readlines()
                    lines_temp = [line.strip() for line in lines_temp if line.strip()]
                    # lines.extend(lines_temp)
                with open("proxies_pass.txt", "r") as f:
                    lines_temp = f.readlines()
                    lines_temp = [line.strip() for line in lines_temp if line.strip()]
                    for i, line in enumerate(lines_temp):
                        splitted = line.split(":")
                        lines_temp[i] = f"http://{splitted[2]}:{splitted[3]}@{splitted[0]}:{splitted[1]}/"
                    # lines.extend(lines_temp)

                with open("megaproxylist.csv", "r") as f:
                    reader = csv.reader(f, delimiter=";")
                    lines_temp = [f"{row[0]}:{row[1]}" for row in reader]
                    del lines_temp[0]
                    del lines_temp[1]
                    # lines.extend(lines_temp)

                # response = requests.get(f"https://api.proxyscrape.com/v2/?request=displayproxies&protocol={self.type_of_proxy}&timeout={10000}&country=all&ssl=all&anonymity=all")
                response = requests.get("https://raw.githubusercontent.com/TheSpeedX/SOCKS-List/master/http.txt")
                if response.status_code == 200:
                    lines_temp = response.text.split("\n")
                    lines_temp = [line.strip() for line in lines_temp if line.strip()]
                    self.proxyrefreshed = True
                    lines.extend(lines_temp)
                    print("Found " + str(len(lines)) + " proxies.")
                    return lines
            except Exception as _:
                time.sleep(1)
                return self.get_proxies()
        elif self.proxyreturned1time is False:
            self.proxyreturned1time = True
            return self.proxylist

    def get_url(self, current_proxy):
        # Retrieve the URL for the channel's stream
        url = ""
        try:
            # self.session.set_option("http-proxy", {"http": f"http://{current_proxy['http']}", "https": f"http://{current_proxy['http']}"})
            streams = self.session.streams(self.channel_url)
        except Exception as e:
            print(e)
            return ""
        try:
            url = streams['audio_only'].url
        except KeyError:
            try:
                url = streams['worst'].url
            except KeyError:
                print("key err")
                url = ""
        if url == "":
            print("no url rec")
            return ""
        return url

    def open_url(self, proxy_data):

        current_proxy = {"http": proxy_data['proxy'], "https": proxy_data['proxy']}

        # Open the stream URL using the given proxy
        headers = {'User-Agent': self.ua.random}
        # current_index = self.all_proxies.index(proxy_data)

        if proxy_data['url'] == "":
            # If the URL is not fetched for the current proxy, fetch it
            proxy_data['url'] = self.get_url(current_proxy)
        if proxy_data['url'] == "":
            self.thread_semaphore.release()  # Release the semaphore
            return
        current_url = proxy_data['url']
        # print(current_url)
        try:
            # if time.time() - proxy_data['time'] >= random.randint(1, 5):
            # Refresh the proxy after a random interval
            current_proxy = {"http": proxy_data['proxy'], "https": proxy_data['proxy']}
            with requests.Session() as s:
                response = self.make_request_with_retry(s, current_url, current_proxy, headers, proxy_data['proxy'], max_retries=1)
            if response:
                self.nb_requests += 1
                # proxy_data['time'] = time.time()
                # self.all_proxies[current_index] = proxy_data
        except Exception as e:
            print(e)
            pass
        finally:
            # print("thread done in " + str(time.time() - start_t))
            self.thread_semaphore.release()  # Release the semaphore

    def stop(self):
        # Stop the ViewerBot by setting the stop event
        self.stop_event = True

    def main(self):
        print("running bot")

        self.proxies = self.get_proxies()
        # self.filter_proxies([{"http": proxy, "https": proxy} for proxy in self.proxies])
        # self.filter_proxies(self.proxies)
        start = datetime.datetime.now()
        self.create_session()
        while not self.stop_event and self.proxies:
            elapsed_seconds = (datetime.datetime.now() - start).total_seconds()
            print("cycle elapsed seconds: " + str(elapsed_seconds))

            self.all_proxies = []
            for p in self.proxies[:min(len(self.proxies), self.max_proxy)]:
                # Add each proxy to the all_proxies list
                self.all_proxies.append({'proxy': p, 'time': time.time(), 'url': ""})

            threads = []
            for i, proxy_data in enumerate(self.all_proxies):
                # Open the URL using a proxy from the all_proxies list
                self.thread_semaphore.acquire()  # Acquire the semaphore
                thread = Thread(target=self.open_url, args=(proxy_data,))
                thread.daemon = True  # This thread dies when the main thread (only non-daemon thread) exits.
                thread.start()
                threads.append(thread)
                time.sleep(1 / self.pps)
                # time.sleep((self.timeout / 1000 / len(self.all_proxies)))

            print("Spawned " + str(len(threads)) + " threads")
            # print("Sleeping now...")
            # time.sleep(self.timeout / 1000)
            if elapsed_seconds >= 300 and not self.proxy_imported:
                # Refresh the self.proxies after 300 seconds (5 minutes)
                start = datetime.datetime.now()
                self.proxies = self.get_proxies()
                elapsed_seconds = 0  # Reset elapsed time
                self.proxyrefreshed = False

    def filter_proxies(self, _proxies):
        out = []
        try:
            for proxy in _proxies:
                response = requests.get("https://twitch.tv", proxies=proxy, timeout=2)
                if response.status_code == 200:
                    print("good proxy: ", proxy)
                    out.append(proxy)
                else:
                    print(response)
        except Exception as e:
            print("failed to filter proxies, ", e)
        print(f"input len = {len(_proxies)}, output len = {len(out)}")
